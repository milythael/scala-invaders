package com.bcmily.scalaInvaders

trait Collision{
  def collidedWith(other: Entity)
}
