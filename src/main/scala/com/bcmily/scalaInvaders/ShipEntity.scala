package com.bcmily.scalaInvaders

class ShipEntity(g:Game,sprite:String,sX:Int,sY:Int) extends Entity(sprite,sX,sY){
  private var game = g

  override def move(delta:Long){
    // if moving left and have reached the left hand side 
    // of the screen stop moving
    if((dx<0) && (x<10)){
      return
    }

    // if moving right and have reached the right hand side
    // of the screen stop moving
    if((dx > 0) && (x>750)){
      return
    }

    super.move(delta)
  }
  override def collidedWith(other:Entity){
    // if it is an alien, notity the game that the player
    // has died
    if ( other.isInstanceOf[AlienEntity]){
      game notifyDeath()
    }
  }
}
