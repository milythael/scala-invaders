package com.bcmily.scalaInvaders

class AlienEntity(g:Game,sprite:String,sX:Int,sY:Int) extends Entity(sprite,sX,sY){
  private var game = g
  private var moveSpeed:Double = 75

  dx = -moveSpeed

  override def move(delta:Long){
    // If the aliens have reached the left hand side of the screen and 
    // are moving left then request a logic update
    if((dx < 0) && (x < 10)){
      game.updateLogic()
    }
    
    // If the aliens have reached the left hand side of the screen and 
    // are moving left then request a logic update
    if((dx > 0) && (x >750)){
      game.updateLogic()
    }

    // proceed with normal move
    super.move(delta)
  }
  
  /**
   * Update the game Logic related to aliens
   */
  override def doLogic(){
    // Swap over horizontal movement and move down the 
    // screen a bit
    dx = -dx
    y = y + 10

    // if the aliens have reached the bottom of the screen
    // then the player dies

    if(y > 570){
      game notifyDeath()
    }
  }
}
