package com.bcmily.scalaInvaders

import java.awt.{Graphics,Image}

class Sprite(startImage:Image){
  // The image to be drawn for this sprite
  private val image = startImage

  /**
  * Get the Width of the sprite
  * @return The width in pixels of this sprite
  */
  def getWidth() = { 
    image getHeight(null)
  }

  /**
  * Get the Height of the drawn sprite
  *
  * @return The Height in pixels of this sprite
  */
  def getHeight() = {
    image getHeight(null)
  }

  /**
  * Draw the sprite onto the graphics context
  *
  * @param g The Graphics context on which to draw the sprite
  * @param x The x Location at which to draw the sprite
  * @param y The y Location at which to draw the sprite
  */
  def draw(g:Graphics,x:Int,y:Int) {
    g drawImage(image,x,y,null)
  }
}
