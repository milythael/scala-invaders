package com.bcmily.scalaInvaders

import java.awt.{Canvas,Color,Dimension,Graphics2D}
import java.awt.event.{KeyAdapter,KeyEvent,WindowAdapter,WindowEvent}
import java.awt.image.BufferStrategy
import java.util.ArrayList

import javax.swing.{JFrame,JPanel}

class Game extends Canvas{
  private var strategy:BufferStrategy = null
  private var gameRunning = true
  private var alienCount = 0
  private var lastFire:Long = 0
  private var ship:Entity = null 
  private var message = ""
  private var waitingForKeyPress = true 
  private var leftPressed = false
  private var rightPressed = false
  private var firePressed = false
  private var logicRequiredThisLoop = false

  private val moveSpeed = 300
  private val fireInterval = 500
  private val entities = new ArrayList[Entity] 
  private val removeList = new ArrayList[Entity]
  private val firingInterval:Long = 500
  
  GameWindow()


  def GameWindow(){
    // create a frame to contain the game
    val container = new JFrame("Scala Invaders")

    // get a hold of the content of the frame and set up the 
    // resolution of the game
    val panel = (container getContentPane()).asInstanceOf[JPanel]
    panel setPreferredSize(new Dimension(800,600))
    panel setLayout(null)

    // Setup the canvas size and put it inot the content of the frame
    setBounds(0,0,800,600)
    panel add(this)

    // Tell AWT not to repaint the canvas
    setIgnoreRepaint(true)

    // make window visible
    container pack()
    container setResizable(false)
    container setVisible(true)
    container addWindowListener ExitGame()

    addKeyListener(new KeyInputHandler())

    // create the buffering strategy which will allow AWT
    // to manage the accelerated graphics
    createBufferStrategy(2)
    strategy = getBufferStrategy()

    // initialise the entities in the game so there is something
    // to see at startup
    initEntities()

  }
  case class ExitGame() extends WindowAdapter{
    override def windowClosing(e: WindowEvent){
      gameRunning = false
      System exit(0)
    }
  }

  case class KeyInputHandler() extends KeyAdapter{
    /** The number of key presses we'v had while waiting for an "any key" press */
    private var pressCount = 1
   
    /**
     * Notification from AWT that a key has been pressed. 
     * Note that a key being pressed is equal to being pushed down but not
     * released. Thats where keyTyped() comes in.
     *
     * @param e The details of the key that was pressed
     */

    override def keyPressed(e:KeyEvent){
      // if we're waiting for an "any key" typed we don't
      // want anything with just a "press"
      if(waitingForKeyPress){
        return
      }

      if((e getKeyCode()) == KeyEvent.VK_LEFT){
        leftPressed = true
      }
      if((e getKeyCode()) == KeyEvent.VK_RIGHT){
        rightPressed = true
      }
      if((e getKeyCode()) == KeyEvent.VK_SPACE){
        firePressed = true
      }
    }

    override def keyReleased(e:KeyEvent){
      // if we're waiting for an "any key" typed we don't
      // want anything with just a "press"
      if(waitingForKeyPress){
        return
      }

      if((e getKeyCode()) == KeyEvent.VK_LEFT){
        leftPressed = false
      }
      if((e getKeyCode()) == KeyEvent.VK_RIGHT){
        rightPressed = false
      }
      if((e getKeyCode()) == KeyEvent.VK_SPACE){
        firePressed = false
      }
    }

    override def keyTyped(e:KeyEvent){
      if(waitingForKeyPress){
        if(pressCount == 1){
          // since we've now recieved out key typed
          // event we can now mark it as such and start 
          // a new game
          waitingForKeyPress = false
          println("waitingForKeyPress now equals "+waitingForKeyPress)
          startGame()
          pressCount = 0
        }
      }

      // if escape was pressed quit the game
      if ((e getKeyChar()) == 27){
        gameRunning = false
        System exit(0)
      }
    }
  }

  /**
   * The Main game loop. This loop is running durning all game
   * play and is responsible for the following activities:
   * <p>
   * - Working out the speed of the game loop to update moves
   * - Moving the game entities
   * - Drawing the screen contents (entities, text)
   * - Updating game events
   * - Checking Input
   */
  def gameLoop(){
    var lastLoopTime = System currentTimeMillis()

    while(gameRunning){
      // work out how long its been since the last update
      // this will be used to calculate how far the enties
      // should move this loop
      val delta = (System currentTimeMillis()) - lastLoopTime
      lastLoopTime = System currentTimeMillis()

      // Get a hold of a graphis context for the accelerated
      // surface and blank it out
      var g = (strategy getDrawGraphics()).asInstanceOf[Graphics2D]
      g setColor(Color black)
      g fillRect(0,0,800,600)
      

      // cycle round asking each entity to move itself
      if(!waitingForKeyPress){
        val it = entities iterator()
        while (it hasNext) {(it next) move(delta) }
      }

      // cycle fround drawing all the enties in the game
      val drawIt = entities iterator()
      while (drawIt hasNext){(drawIt next) draw(g) } 

      // brute force collision, compare every entity against
      // every other entity. If any of them collied notity
      // both entities that the collision has occured
      val entitySize = entities size()
      for(i <- 0 until entitySize){
        for(j <- (i+1) until entitySize){
          val me = (entities get(i)).asInstanceOf[Entity]
          val him = (entities get(j)).asInstanceOf[Entity]
          if(me.getClass != him.getClass){
            if(me.collidesWith(him)){
                me collidedWith(him)
                him collidedWith(me)
            }
          }
        }
      }
      
      entities removeAll(removeList)
      removeList clear()

      // if a game event has indicated that game logic should
      // be resolved, cycle round every entity requesting that
      // their personal logic should be considered
      if(logicRequiredThisLoop){
        val logicI = entities iterator()
        while(logicI hasNext){(logicI next) doLogic()}
        logicRequiredThisLoop = false
      }

      if (waitingForKeyPress) {
        g setColor(Color.white)
        g drawString(message,(800-g.getFontMetrics().stringWidth(message))/2,250)
        g drawString("Press any key",(800-g.getFontMetrics().stringWidth("Press any key"))/2,300)
      }

      // completed drawing so clear up the graphics and 
      // flip the buffer over
      g dispose()
      strategy show()
     
      // resolve the movement of the ship. First assume the ship
      // isn't moving. If either cursor key is pressed then
      // update the movement appropraitely
      ship setHorizontalMovement(0)

      if( leftPressed && !rightPressed){
        ship setHorizontalMovement(-moveSpeed)
      }else if(rightPressed && !leftPressed){
        ship setHorizontalMovement(moveSpeed)
      }

      if(firePressed){
        tryToFire();
      }


      // pause for a bit. Note: this should run at about 100 fps
      try Thread sleep(10) catch { case _ => println _}
    }
  }

  /**
   * Notification from a game entity that the logic of the game
   * should be run at the next opportunity
   */
  def updateLogic(){
    logicRequiredThisLoop = true;
  }

  /**
   * Remove an entity from the game. The entity removed
   * will no longer move or be drawn.
   * 
   * @param entity The entity that should be removed
   */
  def removeEntity(entity:Entity){
    removeList add(entity)
  }

  /**
   * Notification that the player has died.
   */
  def notifyDeath(){
    message = "Oh no! They Got you. Try again?"
    waitingForKeyPress = true;
  }

  /**
   * Notification that the player has won since all the 
   * aliens are dead.
   */
  def notifyWin(){
    message = "Well done! You Win!"
    waitingForKeyPress = true
  }

  /**
   * Notification that an alien has been killed
   */
  def notifyAlienKilled(){
    // reduce the alien count, if there are none left, the player has won!
    alienCount = alienCount - 1
    println("One Aien Down. Now Only "+alienCount+" left")
    if(alienCount == 0){
      notifyWin()
    }

    // if there are still some aliens left then they all need to get faster, so
    // speed up the existing aliens

    val aliens = entities iterator()
    while(aliens hasNext){
      val anEntity = aliens next()
      if(anEntity.isInstanceOf[AlienEntity]){
        // speed up by 2%
        anEntity setHorizontalMovement((anEntity getHorizontalMovement()) * 1.02)
      }
    }
  }

  def tryToFire(){
    // check that we have waited long enough to fire
    if(( (System currentTimeMillis()) - lastFire) < firingInterval){
      return
    }

    //if enough time has passed creat the shot entity and record the time
    lastFire = System currentTimeMillis()
    val shot = new ShotEntity(this,"sprites/shot.gif",ship.getX()+10,ship.getY()-30)
    entities.add(shot)
  }

  private def startGame(){
    entities.clear()
    initEntities()

    // Blank out any keyboard settings we might currently have
    leftPressed = false
    rightPressed = false
    firePressed = false
  }

  private def initEntities(){
    // create the player ship and place it roughly in the center of the screen
    ship = new ShipEntity(this,"sprites/ship.gif",370,550)
    entities add ship
    println("Ships are ready")

    // create a block of aliens( 5 rows, by 12 aliens, spaced evenly)
    alienCount = 0
    for(row <- 0 until 5){
      for(coloumn <- 0 until 12){
        entities add( new AlienEntity(this,"sprites/alien.gif",100+(coloumn*50),(50)+row*30))
        alienCount = alienCount + 1
      }
    }
    println(alienCount+" Aliens to kill. We can do it")
  }
}

object Game extends App {
  println("We are being Invaded")
  val game = new Game()

  game gameLoop()
}
