package com.bcmily.scalaInvaders

import java.awt.{GraphicsConfiguration,GraphicsEnvironment,Image,
                  Transparency}
import java.awt.image.BufferedImage
import java.io.IOException
import java.net.URL
import java.util.HashMap

import javax.imageio.ImageIO

class SpriteStore{

  /* The cached sprite map, from reference to sprite instance */
  private val sprites = new HashMap[String,Sprite]

  def getSprite(ref:String) :Sprite = {
    // If the sprite is already in the cache
    // then just return the existing version
    if( (sprites get(ref)) != null){
      return (sprites get(ref)).asInstanceOf[Sprite]
    }

    var sourceImage:BufferedImage = null

    try{
      // The ClassLoader getResource() ensures we get the sprite
      // from the appropriate place, this helps with deploying the game
      // with things like webstart. 

      val url = this getClass() getClassLoader() getResource(ref)

      if( url == null){
        println("Can't find ref: "+ref);
      }

      // use ImageIO to read the image in
      sourceImage = ImageIO read(url)
    }catch{
      case ioe: IOException => println("Failed to load: "+ref)
    }
    
    // Create an accelerated image of the right size to store the sprite in
    val gc = GraphicsEnvironment getLocalGraphicsEnvironment() getDefaultScreenDevice() getDefaultConfiguration()
    val image = gc createCompatibleImage(sourceImage getWidth(),sourceImage getHeight,Transparency BITMASK)

    // draw source image into the accelerated image
    image getGraphics() drawImage(sourceImage,0,0,null)
    
    // create a sprite, add it to the cache then return it
    val sprite = new Sprite(image)
    sprites put(ref,sprite)

    return sprite
  }

}

object SpriteStore{
  private val single = new SpriteStore()
  /**
  * Get the single Instance of this class
  *
  * @return The Single INstance of this class
  */
  def get() = single
}
