package com.bcmily.scalaInvaders

class ShotEntity(g:Game,sprite:String,sX:Int,sY:Int) extends Entity(sprite,sX,sY){
  /** Shot Vertical Speed **/
  private val moveSpeed:Double = -300
  private val game = g 
  /** True if this shot has been "used", i.e. it hit something */
  private var used:Boolean = false


  /**
   * @param g The game in which the shot has been created
   * @param sprite The sprite representing the shot
   * @param sX The starting x location of the shot
   * @param sY The starting y location of the shot
   */
  
  dy = moveSpeed

  override def move(delta:Long){
    // proceed with normal move
    super.move(delta)

    // if we shot off the screen remove the shot 
    if( y < -100){
      game removeEntity(this)
    }
  }

  override def collidedWith(other:Entity){
    // if we've hit an alien, kill it!!!!
    if(other.isInstanceOf[AlienEntity]){
      // remove the affected entities
      game removeEntity(this)
      game removeEntity(other)

      // notity the game that the alien has been killed
      game notifyAlienKilled()
      used = true
    }
  }
}
