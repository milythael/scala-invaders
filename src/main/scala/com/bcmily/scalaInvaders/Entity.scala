package com.bcmily.scalaInvaders

import java.awt.{Graphics,Rectangle}

class Entity(ref:String,sX:Int,sY:Int) extends Collision {

  /** The current x location of this entity */
  var x = sX.asInstanceOf[Double] 
  /** The current y location of this entity */
  var y = sY.asInstanceOf[Double] 
  /** The sprite that represents this entity */
  var sprite = SpriteStore get() getSprite(ref)
  /** The current speed of this entity horizontally (pix/sec) */
  var dx:Double = 0
  /** The current speed of this entity vertically (pix/sec) */
  var dy:Double = 0
  /** The rectangle used for this entity during collisions */
  private var me = new Rectangle()
  /** The rectangle used for other entity during collisions */
  private var him = new Rectangle()


  def move(delta:Long){
    x = x + (delta * dx)/1000
    y = y + (delta * dy)/1000
  }

  def draw(g:Graphics){
    sprite draw(g,x.asInstanceOf[Int],y.asInstanceOf[Int])
  }
  
  /**
   * Set the horizontal speed of this entity
   *
   *@param dx The horizontal speed of this entity (pixels/sec)
   */
  def setHorizontalMovement(dx:Double){
    this.dx = dx
  }

  /**
   * Get the horizontal speed of this entity
   *
   * @return dx The horizontal speed of this entity
   */
  def getHorizontalMovement():Double = {
    return dx
  }

  /**
   * Set the vertical speed of this entity
   *
   *@param dy The vertical speed of this entity (pixels/sec)
   */
  def setVerticalMovement(dy:Double){
    this.dy = dy
  }

  /**
   * Get the vertical speed of this entity
   *
   * @return dy The vertical speed of this entity
   */
  def getVerticalMovement():Double = {
    return dx
  }

  /**
   * Get the y location of this entity
   *
   * @return The y location of this entity
   */
  def getY():Int = {
    return y.asInstanceOf[Int]
  }

  /**
   * Get the x Location of this entity
   *
   * @return The x location of this entity
   */
  def getX():Int = {
    return x.asInstanceOf[Int]
  }
  
  /**
   * Check if the entity collied with another.
   *
   * @param other The other entity to check collision against
   * @return True if the entities collied with each other
   */
  def collidesWith(other:Entity):Boolean = {
    me setBounds(x.asInstanceOf[Int],y.asInstanceOf[Int],sprite getWidth(),sprite getHeight())
    him setBounds(other.x.asInstanceOf[Int], other.y.asInstanceOf[Int],other.sprite.getWidth(), other.sprite.getHeight())
    
    return me intersects(him)
  }
  def collidedWith(other:Entity){
    
  }
  def doLogic(){

  }
}
